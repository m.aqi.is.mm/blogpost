var express = require('express'),
    postController = require('../controllers/posts')
var router = express.Router();

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.send('<p>HTML Data</p>');
// });

require('../routes/posts')(router, postController);


module.exports = router;

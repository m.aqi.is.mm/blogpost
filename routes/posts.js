module.exports = (router, clientsController) => {

    router.post("/posts", clientsController.addPost);
    router.get("/posts", clientsController.getPost);
    router.get("/posts/:id", clientsController.getPostById);
}
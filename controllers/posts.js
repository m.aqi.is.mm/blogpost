const moduleName = "[posts]",
_ = require("underscore"),
    request = require("request"),
    postsModel = require('../models/posts');
    const { Op } = require("sequelize");


exports.addPost = async(req, res) => {
    try {
        let {isPublished, title, author, timestamp, publishedDate} = req.body;
        // Create a new user
        const post = await postsModel.create({ isPublished : isPublished, title: title, author: author, timestamp : timestamp, publishedDate : publishedDate });
        // console.log("post added : ", post);
        res.status(200).json({ success: true, msg: "post added successfully." })
    } catch (error) {
        res.status(400).json({ success: false, msg: error.message });
    }
}

exports.getPost = async(req, res) => {
    try {
        let { author, isPublished } = req.query;

        let posts = [];
        let authorPosts = [];
        let publish = [];
        if (!_.isEmpty(isPublished)) {
            if ((isPublished == 'true' || isPublished == 'false')) {
                isPublished = Boolean(isPublished);
                console.log(isPublished);
                if (_.isBoolean(isPublished)) {
                    publish = await postsModel.findAll({
                        where: {
                            isPublished: isPublished
                        }
                    });
                }
            }else{
                publish = [];
            }
        } 
        if (!_.isEmpty(author)) {
            authorPosts = await postsModel.findAll({
                where: {
                    author: author
                }
            });
        } else {
            posts = await postsModel.findAll();
        }

        posts = [ ...authorPosts, ...publish, ...posts];

        if (!_.isEmpty(posts)) {
            const arrUniq = [...new Map(posts.map(v => [v.id, v])).values()]
            res.status(200).json({ success: true, posts: arrUniq })
        } else {
            res.status(404).json({ success: false, msg: "No record found." });
        }
    } catch (error) {
        res.status(400).json({ success: false, msg: error.message });
    }
}
exports.getPostById = async (req, res) => {
    try {
        let { id } = req.params;
        id = Number(id);
        let posts = [];
        posts = await postsModel.findAll({
            where: {
                id: id
            }
        });
        if (!_.isEmpty(posts)) {
            res.status(200).json({ success: true, posts: posts })
        } else {
            res.status(404).json({ success: false, msg: "ID not found" });
        }
    } catch (error) {
        res.status(400).json({ success: false, msg: error.message });
    }
}
